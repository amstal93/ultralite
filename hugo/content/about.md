---
title: About Ultralite
author: Jesse Perry
date: "2021-01-01"
menu: main
draft: no
---

This is the first theme I have created for Hugo. I have tried several of the
themes, and there are some really cool ones out there, but I always find some
little thing that just doesn't meet my needs. After quite a bit of fiddling, I
decided what I really wanted was a VERY simple template that I can start from,
something built from scratch. So the `hugo-ultralite` theme was born. 

I have to give credit to the [hugo-xmin](https://github.com/yihui/hugo-xmin)
theme, the author really caught my attention that he was able to produce a
template in about 5hrs. Admittedly, it took me a LOT longer to produce this
template, but it was a worthy endeavor.

## Features

One of the most important inclusions in this theme is the rendering of code
blocks. To do this, I am using the `code fences` feature of Hugo. Below is an
example of code syntax rendering of the the `theme.toml` file. 

```toml
name = "Ultralite"
license = "MIT"
description = "Ultralite hugo theme with minimalist styling and code syntax highlighting"
homepage = "https://gitlab.com/jptechnical/hugo-ultralite"
licenselink = "https://gitlab.com/jptechnical/hugo-ultralite/-/blob/master/LICENSE.md"
tags = ["minimal" ,"blog" ,"clean" ,"minimalist" ,"simple" ,"starter"]
features = ["blog", "code blocks"]
min_version = "0.6"

[author]
  name = "Jesse Perry"
  homepage = "https://gitlab.com/jptechnical/hugo-ultralite"
```
