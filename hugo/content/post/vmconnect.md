---
title: A shortcut to a Hyper-V guest machine
author: Jesse Perry
date: "2021-02-18"
categories:
- HowTos
tags:
- VM
- CMD
draft: no
---

### TL;DR

Create a batch file that runs `vmconnect`, make a shortcut to the batch file,
set the shortcut to run as administrator. Viola!

# How can I make a shortcut to a Hyper-V VM on my desktop for quick access?

Here is my scenario, I have a client that needs an old windows 7 system kept for
historical purposes, and it is easily accessed from his computer. This, in
itself, is not at all unique, I do this all the time. What is unique is that
this client wants access to this legacy system on his computer, and he is a
one-man-shop with only one computer. So on his little Intel NUC desktop, I
enabled the Hyper-V role, used disk2vhd to p2v the old computer, and now it
happily runs in the background. Now... how do I make him a shortcut?
## That is EASY in Virtualbox

I would have thought it would be pretty simple, to make a shortcut to a guest
VM. In fact, if I had used VirtualBox instead, that is a simple right-click and
whammy there is a shortcut on the desktop. On second thought, I probably would
have been better off doing this in VirtualBox rather than Hyper-V, the former
being very consumer friendly, the latter being a bit less so. But, I am in it
now, no time to change to VirtualBox. 

## You can do it from the command prompt with `vmconnect`

With a little research I found `vmconnect.exe` that will bring up a dialog where
I select the host and the guest and connect to the guest instance. In fact, I
*think* that vmconnect is being used when you double-click on the preview window
in Hyper-V and connect to the guest console. I browsed the options by running
`vmconnect /?`, [here is a great
cheatsheet](https://ss64.com/nt/vmconnect.html). The command syntax is
`vmconnect "hostname" "guestname"`. Since this is running on the local machine,
the hostname is `localhost`. Let's say the name of the VM is `Awesome Legacy
VM`, then my full command line is below.

```cmd
vmconnect "localhost" "Awesome Legacy VM"
```

This command above opens up the same remote desktop console as the one you see
from the Hyper-V console. That was EASY! I am well on my way, now I need a
shortcut. 

## Shortcuts in Windows can be finicky, I'll take a .bat file anyday

I would have thought I only had to make a shortcut to `vmconnect.ext` and
include the arguments in the shortcut, but alas it didn't work. This has been my
experience with *.lnk files over the years, so I don't often rely on them.
Instead I created a `AwesomeLegacyVM.bat` file in the folder with the VM to keep
it together, and it included the command above. And then I created a shortcut to
that batch file, then I put it on the desktop. But it didn't work.

## YOU can't do that, you aren't an admin

It turns out, you have to run the `vmconnect` app as administrator, I didn't
realize that because I was always in an administrator command prompt. This was
simple enough to fix, I just changed the shortcut to always run as
administrator. The prompt you get when you run it is the same prompt when you
run a command prompt as administrator, so I will have to tell the client to
expect that. But this worked, I have a shortcut to a VM running in Hyper-V.

## Some refinements

One thing that bugged me is that when I ran the shortcut there would be a
command prompt that was sitting in the background while the connection was
active. This was easy to fix, I changed the script to spawn a new process with
the `vmconnect` program, and then I exited the script at the end. Below is the
final script in all it's minimal glory. 

```batch
start vmconnect "localhost" "Awesome Legacy VM"
exit
```

