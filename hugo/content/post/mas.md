---
title: Install MacOS apps from the cli (beyond homebrew)
author: Jesse Perry
date: "2021-03-08"
categories:
- HowTos
tags:
- MacOS
- CLI
draft: no
---
# How to install apps from the Apple Store on your Mac from the terminal

I am forever in the quest to stop using my mouse for simple tasks. Installing
software is one of those situations... I have been spoiled with the Linux
package managers like `apt` and `yum`, and I get really grumpy when I have to
use my mouse to install something. The [homebrew cli tool](https://brew.sh) has
been a mainstay for me since I started using macs a few years ago, but then I
hit the limit on that sometimes as well. In this case, it is the [Microsoft
Remote
Desktop](https://apps.apple.com/us/app/microsoft-remote-desktop/id1295203466?mt=12)
app that is the most recent source of clickiness. Let's remedy that.

## Mas comes to the rescue

Thanks to the [mas](https://github.com/mas-cli/mas) project, we can install apps
from the Apple Store with the terminal. First, you need to install it, using
`brew install mas`, naturally. Then you need to sign-in to the Apple Store, this
bit of clickiness is unavoidable since the `mas signin` is broken as of MacOS 10.13 according to [this github issue](https://github.com/mas-cli/mas/issues/164). 

Finally, you can install your app by using the ID in it's name. You can find
apps with `mas search microsoft | grep -i remote`, I know it has both
'Microsoft' and 'Remote' in the name, so I grep it. Now that I found the ID, I
can install it with 'mas install 1295203466'. Viola, we have Microsoft RDP
installed. Now I can use it with `vagrant rdp`, and all those other places I
need to RDP to something. 

```bash
$ mas search microsoft | grep -i remote
  1295203466  Microsoft Remote Desktop                                                               (10.5.1)
$ mas install 1295203466
==> Downloading Microsoft Remote Desktop
==> Installed Microsoft Remote Desktop
```
